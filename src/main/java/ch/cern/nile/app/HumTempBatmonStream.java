package ch.cern.nile.app;

import java.util.HashMap;
import java.util.Map;

import ch.cern.nile.app.generated.HumTempBatmonPacket;
import ch.cern.nile.kaitai.streams.LoraDecoderStream;

public final class HumTempBatmonStream extends LoraDecoderStream<HumTempBatmonPacket> {

    private static final Map<String, String> CUSTOM_METADATA_FIELD_MAP = new HashMap<>();

    static {
        CUSTOM_METADATA_FIELD_MAP.put("adr", "adr");
        CUSTOM_METADATA_FIELD_MAP.put("fCnt", "fCnt");
        CUSTOM_METADATA_FIELD_MAP.put("fPort", "fPort");
    }

    public HumTempBatmonStream() {
        super(HumTempBatmonPacket.class, CUSTOM_METADATA_FIELD_MAP, false, true, true);
    }
}
