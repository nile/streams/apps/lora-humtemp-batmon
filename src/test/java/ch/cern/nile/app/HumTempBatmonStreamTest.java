package ch.cern.nile.app;

import static org.junit.jupiter.api.Assertions.assertEquals;

import com.google.gson.JsonObject;

import org.apache.kafka.clients.producer.ProducerRecord;
import org.junit.jupiter.api.Test;

import ch.cern.nile.common.streams.AbstractStream;
import ch.cern.nile.test.utils.StreamTestBase;
import ch.cern.nile.test.utils.TestUtils;

public class HumTempBatmonStreamTest extends StreamTestBase {

    private static final JsonObject DATA_FRAME = TestUtils.loadRecordAsJson("data/data_frame.json");
    private static final JsonObject DATA_FRAME_NAN_BRIDGE_VOLTAGE =
            TestUtils.loadRecordAsJson("data/data_frame_nan_bridge_voltage.json");

    @Override
    public AbstractStream createStreamInstance() {
        return new HumTempBatmonStream();
    }

    @Test
    void givenDataFrame_whenDecoding_thenCorrectOutputRecordIsCreated() {
        pipeRecord(DATA_FRAME);
        final ProducerRecord<String, JsonObject> outputRecord = readRecord();

        assertEquals(255, outputRecord.value().get("dummyByte").getAsInt());
        assertEquals(991, outputRecord.value().get("extwtdCnt").getAsInt());
        assertEquals(0, outputRecord.value().get("floodConnection1").getAsInt());
        assertEquals(1, outputRecord.value().get("floodConnection2").getAsInt());
        assertEquals(1, outputRecord.value().get("floodConnection3").getAsInt());
        assertEquals(0, outputRecord.value().get("floodReading1").getAsInt());
        assertEquals(3, outputRecord.value().get("floodReading2").getAsInt());
        assertEquals(3, outputRecord.value().get("floodReading3").getAsInt());
        assertEquals(128, outputRecord.value().get("humiCap0").getAsInt());
        assertEquals(0, outputRecord.value().get("humiSumPeriod").getAsInt());
        assertEquals(3.31611328125, outputRecord.value().get("mon33").getAsDouble());
        assertEquals(5.0096191406249995, outputRecord.value().get("mon5").getAsDouble());
        assertEquals(198, outputRecord.value().get("packetNumber").getAsInt());
        assertEquals(0, outputRecord.value().get("statusFlag").getAsInt());
        assertEquals(255, outputRecord.value().get("swbuild").getAsInt());
        assertEquals(1.4542236328125, outputRecord.value().get("temperatureAdcConv").getAsDouble());
        assertEquals(1.7783251231527093, outputRecord.value().get("temperatureBridgeVoltage").getAsDouble());
        assertEquals(34542.6289157997, outputRecord.value().get("temperatureGainAdcConv").getAsDouble());
        assertEquals(0, outputRecord.value().get("temperatureDpConnection1").getAsInt());
        assertEquals(1015, outputRecord.value().get("tempGain").getAsInt());
        assertEquals(1805, outputRecord.value().get("tempRawValue").getAsInt());
        assertEquals(1.326714509544618, outputRecord.value().get("vBat").getAsDouble());
    }

    @Test
    void givenDataFrameWithNaNBridgeVoltage_whenDecoding_thenOutputRecordWithZeroBridgeVoltageIsCreated() {
        pipeRecord(DATA_FRAME_NAN_BRIDGE_VOLTAGE);
        final ProducerRecord<String, JsonObject> outputRecord = readRecord();
        assertEquals(0.0, outputRecord.value().get("temperatureBridgeVoltage").getAsDouble());
    }
}
