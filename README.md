# LoRa HumTemp Batmon

## Overview

### Owners & Administrators

| Title       | Details                                                                                              |
|-------------|------------------------------------------------------------------------------------------------------|
| **E-group** | [BE-cern-epr-batmon-admin](https://groups-portal.web.cern.ch/group/BE-cern-epr-batmon-admin/details) |
| **People**  | [Alessandro Zimmaro](https://phonebook.cern.ch/search?q=Alessandro+Zimmaro)                          |
|             | [Salvatore Danzeca](https://phonebook.cern.ch/search?q=Salvatore+Danzeca)                            |

### Kafka Topics

| Environment    | Topic Name                                                                                                              |
|----------------|-------------------------------------------------------------------------------------------------------------------------|
| **Production** | [lora-humtemp-batmon](https://nile-kafka-ui.app.cern.ch/ui/clusters/gp3/all-topics/lora-humtemp-batmon)                 |
| **Production** | [lora-humtemp-batmon-decoded](https://nile-kafka-ui.app.cern.ch/ui/clusters/gp3/all-topics/lora-humtemp-batmon-decoded) |
| **QA**         | [lora-humtemp-batmon](https://nile-kafka-ui.app.cern.ch/ui/clusters/qa3/all-topics/lora-humtemp-batmon)                 |
| **QA**         | [lora-humtemp-batmon-decoded](https://nile-kafka-ui.app.cern.ch/ui/clusters/qa3/all-topics/lora-humtemp-batmon-decoded) |

### Configuration

| Title                        | Details                                                                                                |
|------------------------------|--------------------------------------------------------------------------------------------------------|
| **Application Name**         | BE-cern-epr-Hum-Temp-batmon                                                                            |
| **Configuration Repository** | [app-configs/lora-humtemp-batmon](https://gitlab.cern.ch/nile/streams/app-configs/lora-humtemp-batmon) |

### Technologies

- LoRa